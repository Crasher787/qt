#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QString>
#include <QPixmap>
#include <QImage>
#include <QMainWindow>
#include <openssl/sha.h>
#include<QCryptographicHash>
#include <QObject>
#include <QWidget>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
//макс.символов, которые мы вписываем
    char hash[30];

        QString getStringFromUnsignedChar(const unsigned char *str)
        {

            QString s;
            QString result = "";
            int rev = strlen(reinterpret_cast<const char *>(str));

            // Print String in Reverse order....
            for ( int i = 0; i<rev; i++)
                {
                   s = QString("%1").arg(str[i],0,16);

                   if(s == "0"){
                      s="00";
                     }
                 result.append(s);

                 }
           return result;
        }


        unsigned char* read(const char* filename, int &width, int &height, unsigned char info[54])
            {
                int i;
                FILE* f;
                fopen_s(&f, filename, "rb");
                if (!f)
                {

                    exit(1);
                }

                fread(info, sizeof(unsigned char), 54, f);

                width = *(int*)&info[18];
                height = *(int*)&info[22];

                int size = 3 * width * height;
                unsigned char* data = new unsigned char[size];
                fread(data, sizeof(unsigned char), size, f);
                fclose(f);

                return data;
            }

            void write(const char* filename, int width, int height, unsigned char info[54], unsigned char*data)
            {
                int i;
                FILE* f;
                fopen_s(&f, filename, "wb");
                fwrite(info, sizeof(unsigned char), 54, f);

                int size = 3 * width * height;

                fwrite(data, sizeof(unsigned char), size, f);
                fclose(f);

            }

            void modifyB(unsigned char &n, int p, int b)
            {
                int mask = 1 << p;
                n = (n & ~mask) | ((b << p) & mask);
            }



private slots:


            void on_pushButton_clicked();

            void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
