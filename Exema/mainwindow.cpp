#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtMath>

#define INPUT_FILE  "C://Users//victo//Desktop//2.bmp"
#define OUTPUT_FILE  "C://Users//victo//Desktop//fdf//2too.bmp"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QPixmap img1( INPUT_FILE );
    ui->label->setPixmap (img1);
    ui->label->setScaledContents( true );
    ui->label->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );
}

MainWindow::~MainWindow()
{
    delete ui;
}




void MainWindow::on_pushButton_clicked()
{
    QString lanEdit_FIO = ui->lineEdit->text();

   // ui->label->setText(lanEdit_FIO);
   // ui->pushButton->setDisabled(1);

    const unsigned char *ibuf = reinterpret_cast<const unsigned char *>(lanEdit_FIO.toStdString().c_str());
    unsigned char obuf[20];
    unsigned char *res;


    res = SHA1(ibuf, lanEdit_FIO.length(), obuf);

    QString hash = QString("%1").arg(QString(QCryptographicHash::hash(lanEdit_FIO.toUtf8(),QCryptographicHash::Sha1).toHex()));
    ui->label_3->setText(hash);

    for(int i = 0; i < 20; i++)
        hash[i] = obuf[i];
}


void MainWindow::on_pushButton_2_clicked()
{
    QByteArray hashed = QByteArray::fromStdString(sha(ui->editline->text()));
    //QByteArray hashed = QCryptographicHash::hash(start, QCryptographicHash::Sha1).toHex();
    QPixmap img("C:\\Local Rep - term 2\\exam\\18.bmp");
    QImage img1 = img.toImage();
    QImage img2(img1);
    int positions[20][8]={{103328, 331600, 242176, 470887, 110931, 242606, 440098,
                           119285, 181339, 78080, 72864,98193, 8804, 75794, 33501,
                           45951, 158220, 92620, 16213, 188419, 201235, 396255, 345331,
                           60407, 89165, 313056, 424343, 159287, 97380, 235778, 322592,
                           431297, 209970, 47103,198645, 159055, 163228, 121350, 428340,
                           93754, 21192, 387608, 38183, 143305, 46516, 289272, 16475,
                           14288, 131586, 96264, 249170, 378374, 140308, 123759, 184601, 113985,
                           237435, 136641, 273300, 308991, 33846, 145106, 34120, 415383, 323672, 23129, 302089,
                           41840, 150253, 307563, 107309, 101235, 412019, 198038, 226583, 372183, 246406, 425724,
                           168986, 80847, 171292, 463925, 378868, 222013, 120632, 398621, 150463, 113096, 13941,
                           411191, 459102, 233984, 195184, 185350, 224140, 364860, 270817, 373227, 369119, 67556,
                           105965, 79954, 69067, 67647, 394847, 334238, 412801, 279710, 22846, 464958, 97686,
                           411616, 204292, 455665, 346075, 139196, 459415, 232469, 78260, 276319, 457712, 125696,
                           446845, 101015, 12179, 347718, 59986, 177710, 401601, 254894, 10127, 93581, 112811,
                           225686, 191926, 467939, 246320, 455715, 327792, 38076, 386440, 405879, 200244, 231727,
                           293660, 98473, 101033, 179136, 191105, 50416, 96014, 126102, 240391, 171127, 436101,
                           453367, 240135, 384505, 92933, 282641};
    for (int i=0; i<20; i++) {
    char b = hashed[i];
    int bit;
    for (int j=0; j<8; j++) {
    if(b & (1«j)) {bit = 1;} else {bit = 0;} //проверяется что установлен бит номер j
    int pos = positions[i][j];
    int base = pos / 3;
    int offset = pos %3;
    //pos/=3; //в bmp 3 байта на 1 точку
    int w = img1.width();
    //int h = img1.height();
    int y = base / w;
    int x = base - y*w;
    QRgb rgb = img1.pixel(x, y);
    //rgb = ~0; //инверсия битов
    //rgb |= bit; //битовое или
    rgb &= ~(1«(8*offset));
    rgb |= bit«(8*offset);//« - сдвиг битов влево
    img2.setPixel(x,y, rgb);
    }
    }

}
