/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_24;
    QLabel *label_6;
    QLineEdit *lineEdit_16;
    QLineEdit *lineEdit_6;
    QLineEdit *lineEdit_4;
    QLabel *label_19;
    QLineEdit *lineEdit_13;
    QLineEdit *lineEdit_9;
    QLabel *label_8;
    QLabel *label_10;
    QLabel *label_20;
    QLineEdit *lineEdit_7;
    QLabel *label_18;
    QLabel *label_12;
    QLineEdit *lineEdit_12;
    QLabel *label_16;
    QLabel *label_17;
    QLineEdit *lineEdit_10;
    QLineEdit *lineEdit_11;
    QLabel *label_7;
    QLabel *label_22;
    QLabel *label_23;
    QLabel *label_14;
    QLineEdit *lineEdit_15;
    QLineEdit *lineEdit;
    QLabel *label_9;
    QLineEdit *lineEdit_8;
    QLineEdit *lineEdit_2;
    QLabel *label_21;
    QLineEdit *lineEdit_14;
    QLineEdit *lineEdit_5;
    QLabel *label_15;
    QLineEdit *lineEdit_3;
    QLabel *label_13;
    QLabel *label_5;
    QLabel *label_11;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_7;
    QWidget *layoutWidget3;
    QHBoxLayout *horizontalLayout;
    QTableWidget *tableWidget;
    QTableWidget *tableWidget_2;
    QTableWidget *tableWidget_3;
    QTableWidget *tableWidget_4;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QString::fromUtf8("Dialog"));
        Dialog->resize(1211, 697);
        label = new QLabel(Dialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(130, 20, 121, 20));
        label_2 = new QLabel(Dialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(380, 20, 111, 20));
        label_3 = new QLabel(Dialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(646, 20, 111, 20));
        label_4 = new QLabel(Dialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(890, 20, 151, 20));
        layoutWidget = new QWidget(Dialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 460, 671, 213));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_24 = new QLabel(layoutWidget);
        label_24->setObjectName(QString::fromUtf8("label_24"));

        gridLayout->addWidget(label_24, 14, 8, 1, 1);

        label_6 = new QLabel(layoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 3, 0, 1, 1);

        lineEdit_16 = new QLineEdit(layoutWidget);
        lineEdit_16->setObjectName(QString::fromUtf8("lineEdit_16"));

        gridLayout->addWidget(lineEdit_16, 15, 8, 1, 3);

        lineEdit_6 = new QLineEdit(layoutWidget);
        lineEdit_6->setObjectName(QString::fromUtf8("lineEdit_6"));

        gridLayout->addWidget(lineEdit_6, 8, 3, 1, 2);

        lineEdit_4 = new QLineEdit(layoutWidget);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));

        gridLayout->addWidget(lineEdit_4, 3, 8, 2, 3);

        label_19 = new QLabel(layoutWidget);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        gridLayout->addWidget(label_19, 10, 5, 1, 1);

        lineEdit_13 = new QLineEdit(layoutWidget);
        lineEdit_13->setObjectName(QString::fromUtf8("lineEdit_13"));

        gridLayout->addWidget(lineEdit_13, 15, 1, 1, 2);

        lineEdit_9 = new QLineEdit(layoutWidget);
        lineEdit_9->setObjectName(QString::fromUtf8("lineEdit_9"));

        gridLayout->addWidget(lineEdit_9, 11, 1, 2, 2);

        label_8 = new QLabel(layoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout->addWidget(label_8, 11, 0, 1, 1);

        label_10 = new QLabel(layoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout->addWidget(label_10, 2, 3, 1, 1);

        label_20 = new QLabel(layoutWidget);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        gridLayout->addWidget(label_20, 10, 8, 1, 1);

        lineEdit_7 = new QLineEdit(layoutWidget);
        lineEdit_7->setObjectName(QString::fromUtf8("lineEdit_7"));

        gridLayout->addWidget(lineEdit_7, 8, 5, 1, 3);

        label_18 = new QLabel(layoutWidget);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        gridLayout->addWidget(label_18, 10, 3, 1, 1);

        label_12 = new QLabel(layoutWidget);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout->addWidget(label_12, 2, 8, 1, 1);

        lineEdit_12 = new QLineEdit(layoutWidget);
        lineEdit_12->setObjectName(QString::fromUtf8("lineEdit_12"));

        gridLayout->addWidget(lineEdit_12, 11, 8, 2, 3);

        label_16 = new QLabel(layoutWidget);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout->addWidget(label_16, 6, 8, 1, 1);

        label_17 = new QLabel(layoutWidget);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        gridLayout->addWidget(label_17, 10, 1, 1, 1);

        lineEdit_10 = new QLineEdit(layoutWidget);
        lineEdit_10->setObjectName(QString::fromUtf8("lineEdit_10"));

        gridLayout->addWidget(lineEdit_10, 11, 3, 2, 2);

        lineEdit_11 = new QLineEdit(layoutWidget);
        lineEdit_11->setObjectName(QString::fromUtf8("lineEdit_11"));

        gridLayout->addWidget(lineEdit_11, 11, 5, 2, 3);

        label_7 = new QLabel(layoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 8, 0, 1, 1);

        label_22 = new QLabel(layoutWidget);
        label_22->setObjectName(QString::fromUtf8("label_22"));

        gridLayout->addWidget(label_22, 14, 3, 1, 1);

        label_23 = new QLabel(layoutWidget);
        label_23->setObjectName(QString::fromUtf8("label_23"));

        gridLayout->addWidget(label_23, 14, 5, 1, 1);

        label_14 = new QLabel(layoutWidget);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout->addWidget(label_14, 6, 3, 1, 1);

        lineEdit_15 = new QLineEdit(layoutWidget);
        lineEdit_15->setObjectName(QString::fromUtf8("lineEdit_15"));

        gridLayout->addWidget(lineEdit_15, 15, 5, 1, 3);

        lineEdit = new QLineEdit(layoutWidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        gridLayout->addWidget(lineEdit, 3, 1, 2, 2);

        label_9 = new QLabel(layoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout->addWidget(label_9, 14, 0, 2, 1);

        lineEdit_8 = new QLineEdit(layoutWidget);
        lineEdit_8->setObjectName(QString::fromUtf8("lineEdit_8"));

        gridLayout->addWidget(lineEdit_8, 8, 8, 1, 3);

        lineEdit_2 = new QLineEdit(layoutWidget);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));

        gridLayout->addWidget(lineEdit_2, 3, 3, 2, 2);

        label_21 = new QLabel(layoutWidget);
        label_21->setObjectName(QString::fromUtf8("label_21"));

        gridLayout->addWidget(label_21, 14, 1, 1, 1);

        lineEdit_14 = new QLineEdit(layoutWidget);
        lineEdit_14->setObjectName(QString::fromUtf8("lineEdit_14"));

        gridLayout->addWidget(lineEdit_14, 15, 3, 1, 2);

        lineEdit_5 = new QLineEdit(layoutWidget);
        lineEdit_5->setObjectName(QString::fromUtf8("lineEdit_5"));

        gridLayout->addWidget(lineEdit_5, 8, 1, 1, 2);

        label_15 = new QLabel(layoutWidget);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout->addWidget(label_15, 6, 5, 1, 1);

        lineEdit_3 = new QLineEdit(layoutWidget);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));

        gridLayout->addWidget(lineEdit_3, 3, 5, 2, 3);

        label_13 = new QLabel(layoutWidget);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout->addWidget(label_13, 6, 1, 1, 1);

        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 2, 1, 1, 1);

        label_11 = new QLabel(layoutWidget);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 2, 5, 1, 1);

        layoutWidget1 = new QWidget(Dialog);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(1110, 110, 82, 54));
        gridLayout_4 = new QGridLayout(layoutWidget1);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        pushButton_5 = new QPushButton(layoutWidget1);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        gridLayout_4->addWidget(pushButton_5, 0, 0, 1, 1);

        pushButton_6 = new QPushButton(layoutWidget1);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));

        gridLayout_4->addWidget(pushButton_6, 1, 0, 1, 1);

        layoutWidget2 = new QWidget(Dialog);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(680, 460, 101, 211));
        verticalLayout = new QVBoxLayout(layoutWidget2);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(layoutWidget2);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout->addWidget(pushButton);

        pushButton_7 = new QPushButton(Dialog);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        pushButton_7->setGeometry(QRect(1110, 10, 80, 21));
        layoutWidget3 = new QWidget(Dialog);
        layoutWidget3->setObjectName(QString::fromUtf8("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(11, 51, 1091, 391));
        horizontalLayout = new QHBoxLayout(layoutWidget3);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        tableWidget = new QTableWidget(layoutWidget3);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));

        horizontalLayout->addWidget(tableWidget);

        tableWidget_2 = new QTableWidget(layoutWidget3);
        tableWidget_2->setObjectName(QString::fromUtf8("tableWidget_2"));

        horizontalLayout->addWidget(tableWidget_2);

        tableWidget_3 = new QTableWidget(layoutWidget3);
        tableWidget_3->setObjectName(QString::fromUtf8("tableWidget_3"));

        horizontalLayout->addWidget(tableWidget_3);

        tableWidget_4 = new QTableWidget(layoutWidget3);
        tableWidget_4->setObjectName(QString::fromUtf8("tableWidget_4"));

        horizontalLayout->addWidget(tableWidget_4);


        retranslateUi(Dialog);

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QApplication::translate("Dialog", "Dialog", nullptr));
        label->setText(QApplication::translate("Dialog", "\320\241\320\277\320\270\321\201\320\276\320\272 \321\200\320\265\320\263\320\270\320\276\320\275\320\276\320\262 ", nullptr));
        label_2->setText(QApplication::translate("Dialog", "\321\201\320\277\320\270\321\201\320\276\320\272 \320\277\321\200\320\265\320\264\320\277\321\200\320\270\321\217\321\202\320\270\320\271", nullptr));
        label_3->setText(QApplication::translate("Dialog", "\321\201\320\277\320\270\321\201\320\276\320\272 \320\277\320\276\320\272\320\260\320\267\320\260\321\202\320\265\320\273\320\265\320\271", nullptr));
        label_4->setText(QApplication::translate("Dialog", "\320\266\321\203\321\200\320\275\320\260\320\273 \321\203\321\207\320\265\321\202\320\260 \320\264\320\260\320\275\320\275\321\213\321\205", nullptr));
        label_24->setText(QApplication::translate("Dialog", "\320\267\320\260\321\202\321\200\320\260\321\202\321\213", nullptr));
        label_6->setText(QApplication::translate("Dialog", "\320\241\320\277\320\270\321\201\320\276\320\272 \321\200\320\265\320\263\320\270\320\276\320\275\320\276\320\262 ", nullptr));
        label_19->setText(QApplication::translate("Dialog", "\320\232\320\276\320\273\320\270\321\207\320\265\321\201\321\202\320\262\320\276", nullptr));
        label_8->setText(QApplication::translate("Dialog", "\321\201\320\277\320\270\321\201\320\276\320\272 \320\277\320\276\320\272\320\260\320\267\320\260\321\202\320\265\320\273\320\265\320\271", nullptr));
        label_10->setText(QApplication::translate("Dialog", "\320\235\320\260\320\267\320\262\320\260\320\275\320\270\320\265", nullptr));
        label_20->setText(QApplication::translate("Dialog", "\320\224\320\276\321\205\320\276\320\264\321\213", nullptr));
        label_18->setText(QApplication::translate("Dialog", "\320\237\321\200\320\276\320\264\321\203\320\272\321\206\320\270\321\217", nullptr));
        label_12->setText(QApplication::translate("Dialog", "\320\240\320\260\321\201\320\277\320\276\320\273\320\276\320\266\320\265\320\275\320\270\320\265", nullptr));
        label_16->setText(QApplication::translate("Dialog", "\320\240\320\260\321\201\320\277\320\276\320\273\320\276\320\266\320\265\320\275\320\270\320\265", nullptr));
        label_17->setText(QApplication::translate("Dialog", "ID", nullptr));
        label_7->setText(QApplication::translate("Dialog", "\321\201\320\277\320\270\321\201\320\276\320\272 \320\277\321\200\320\265\320\264\320\277\321\200\320\270\321\217\321\202\320\270\320\271", nullptr));
        label_22->setText(QApplication::translate("Dialog", "\320\230\320\275\321\206\320\270\320\264\320\265\320\275\321\202\321\213", nullptr));
        label_23->setText(QApplication::translate("Dialog", "\320\274\320\265\321\201\321\202\320\276", nullptr));
        label_14->setText(QApplication::translate("Dialog", "\320\235\320\260\320\267\320\262\320\260\320\275\320\270\320\265", nullptr));
        label_9->setText(QApplication::translate("Dialog", "\320\266\321\203\321\200\320\275\320\260\320\273 \321\203\321\207\320\265\321\202\320\260 \320\264\320\260\320\275\320\275\321\213\321\205", nullptr));
        label_21->setText(QApplication::translate("Dialog", "ID", nullptr));
        label_15->setText(QApplication::translate("Dialog", "\320\240\320\265\320\271\321\202\320\270\320\275\320\263", nullptr));
        label_13->setText(QApplication::translate("Dialog", "ID", nullptr));
        label_5->setText(QApplication::translate("Dialog", "ID", nullptr));
        label_11->setText(QApplication::translate("Dialog", "\320\240\320\265\320\271\321\202\320\270\320\275\320\263", nullptr));
        pushButton_5->setText(QApplication::translate("Dialog", "\320\277\320\276\320\272\320\260\320\267\320\260\321\202\321\214", nullptr));
        pushButton_6->setText(QApplication::translate("Dialog", "\321\203\320\264\320\260\320\273\320\270\321\202\321\214 ", nullptr));
        pushButton->setText(QApplication::translate("Dialog", "\320\264\320\276\320\261\320\260\320\262\320\270\321\202\321\214", nullptr));
        pushButton_7->setText(QApplication::translate("Dialog", " \320\222 \320\274\320\265\320\275\321\216", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
