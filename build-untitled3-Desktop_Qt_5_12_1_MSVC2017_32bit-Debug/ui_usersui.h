/********************************************************************************
** Form generated from reading UI file 'usersui.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERSUI_H
#define UI_USERSUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UsersUi
{
public:
    QWidget *widget;
    QFormLayout *formLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QTableWidget *tableWidget;

    void setupUi(QDialog *UsersUi)
    {
        if (UsersUi->objectName().isEmpty())
            UsersUi->setObjectName(QString::fromUtf8("UsersUi"));
        UsersUi->resize(530, 267);
        widget = new QWidget(UsersUi);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(20, 10, 501, 248));
        formLayout = new QFormLayout(widget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(widget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        formLayout->setWidget(1, QFormLayout::LabelRole, pushButton);

        pushButton_2 = new QPushButton(widget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        formLayout->setWidget(2, QFormLayout::LabelRole, pushButton_2);

        tableWidget = new QTableWidget(widget);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));

        formLayout->setWidget(0, QFormLayout::SpanningRole, tableWidget);


        retranslateUi(UsersUi);

        QMetaObject::connectSlotsByName(UsersUi);
    } // setupUi

    void retranslateUi(QDialog *UsersUi)
    {
        UsersUi->setWindowTitle(QApplication::translate("UsersUi", "Dialog", nullptr));
        pushButton->setText(QApplication::translate("UsersUi", "\320\237\321\200\320\276\321\201\320\274\320\276\321\202\321\200", nullptr));
        pushButton_2->setText(QApplication::translate("UsersUi", "\320\243\320\264\320\260\320\273\320\265\320\275\320\270\320\265 ", nullptr));
    } // retranslateUi

};

namespace Ui {
    class UsersUi: public Ui_UsersUi {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERSUI_H
