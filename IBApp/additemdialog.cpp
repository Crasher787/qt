#include "additemdialog.h"
#include "ui_additemdialog.h"

AddItemDialog::AddItemDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddItemDialog)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL (clicked()), this, SLOT (on_pushButton_clicked())); // Вешаем обработчик событий на кнопку
    connect(ui->pushButton_2, SIGNAL (clicked()), this, SLOT (on_pushButton_2_clicked())); // Вешаем обработчик событий на кнопку
}

AddItemDialog::~AddItemDialog()
{
    delete ui;
}

StorageItem AddItemDialog::GetInputData()
{
    return inputItem;
}

void AddItemDialog::on_pushButton_clicked()
{
    inputItem.SetTitle(ui->lineEdit->text());
    inputItem.SetSupplier(ui->lineEdit_2->text());
    inputItem.SetReciever(ui->lineEdit_3->text());
    QString keeper = ui->lineEdit_4->text();
    keeper[0] = keeper[0].toUpper();
    inputItem.SetKeeper(keeper);

    this->accept();
}

void AddItemDialog::on_pushButton_2_clicked()
{
    this->reject();
}
