#ifndef JURNALTABLEVIEW_H
#define JURNALTABLEVIEW_H


#include <QtGui>

#include <vector>

using namespace std;

class JurnalItem;
class JurnalTableView : public QAbstractTableModel
{
public:
    JurnalTableView(vector<JurnalItem *> *items);
    int rowCount (const QModelIndex &parent = QModelIndex()) const;
    int columnCount (const QModelIndex &parent = QModelIndex()) const;
    QVariant data (const QModelIndex & index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation,int role = Qt::DisplayRole) const;
    void beginResetModel_();
    void endResetModel_();

private:
    vector<JurnalItem *> *_items;
};
#endif // JURNALTABLEVIEW_H
