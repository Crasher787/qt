#ifndef USER_H
#define USER_H


#include <QString>

class QJsonObject;
class User
{
private:
    int _id;
    QString _login;
    QString _pas;
    QString _region;
    QString _pred;
    QString _nampred;
    QString _prod;
    QString _jurnal;
public:
    User() {}
    User(int id, QString login, QString pas, QString region, QString pred,
            QString nampred, QString prod, QString jurnal);
    User(const QJsonObject &json);
    int id() const { return _id; }
    QString login() const { return _login; }
    QString pas() const { return _pas; }
    QString region() const { return _region; }
    QString pred() const { return _pred; }
    QString nampred() const { return _nampred; }
    QString prod() const { return _prod; }
    QString jurnal() const { return _jurnal; }
    void id(int value) { _id = value; }
    void login(QString value) { _login = value; }
    void pas(QString value) { _pas = value; }
    void region(QString value) { _region = value; }
    void pred(QString value) { _pred = value; }
    void nampred(QString value) { _nampred = value; }
    void prod(QString value) { _prod = value; }
    void jurnal(QString value) { _jurnal = value; }
    void toJson(QJsonObject &res);
};

#endif // USER_H
