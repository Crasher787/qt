#ifndef ADMINTABLEVIEW_H
#define ADMINTABLEVIEW_H


#include <QModelIndex>
#include<QString>
#include <vector>
#include <admin.h>
#include <QAbstractTableModel>

using namespace std;


class AdminTAbleView : public QAbstractTableModel
{
public:
    AdminTAbleView(vector<admin *> *admins);
    int rowCount (const QModelIndex &usr = QModelIndex()) const;
    int columnCount (const QModelIndex &usr = QModelIndex()) const;
    QVariant data (const QModelIndex & index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation,int role = Qt::DisplayRole) const;
    admin* _data(int row);
    void fregion(const QString region);
    void fpred(const QString pred);
    void jurnal(const QString jun);
    void reset();
    void resetFilter();
    void beginResetModel_();
    void endResetModel_();
private:
    std::vector<unsigned int> positions;
    bool isFilter = false;
    vector<admin *> *_admins;
};

#endif // ADMINTABLEVIEW_H
