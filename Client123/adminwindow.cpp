#include "adminwindow.h"
#include "ui_adminwindow.h"
#include <QtGui>
#include <QMessageBox>
#include <iostream>
#include "addchange.h"
#include "client.h"
#include "user.h"
#include "jurnal.h"

AdminWindow::AdminWindow(QWidget *parent):
    QWidget(parent),
    ui(new Ui::AdminWindow)
{

    model = new UserTableView(&_users);
    sortmodel = new QSortFilterProxyModel();
    sortmodel->setSourceModel(model);
    ui->ptable->setModel(sortmodel);
    ui->ptable->setSortingEnabled(true);
    user<<_region<<_pred<<_jurnal<<_painp1;
    ui->pcomboBox->addItems(user);
    //ui->pcomboBox->setCurrentIndex(0);
    this->setWindowTitle("Admin");
    connect(Client::instance(), SIGNAL(jsonRetrieved(const QJsonDocument &)),
            this, SLOT(processJson(const QJsonDocument &)));
}

void AdminWindow::loadList() {
    QJsonObject obj;
    obj.insert("action", "list_users");
    obj.insert("my_id", Client::instance()->getID());
    QJsonDocument json(obj);
    Client::instance()->send(json);
    qInfo() << "list_users\n";
}

void AdminWindow::processJson(const QJsonDocument &json) {
    qInfo() << "started\n";
    model->beginResetModel_();
    QJsonObject o = json.object();
    qInfo() << o["action"].toString() << "\n";
    if (o["action"].toString() != "list_users") {
        return;
    }
    QJsonArray a = o["items"].toArray();
    qInfo() << a.size() << "\n";
    clearPatients();
    for (const QJsonValue &v : a) {
        QJsonObject jsonItem = v.toObject();
        _users.push_back(new User(jsonItem));
    }
    model->endResetModel_();
    ui->ptable->resizeColumnsToContents();
}

void AdminWindow::clearPatients() {
    for (User *v : _users) {
        delete v;
    }
    _users.clear();
}


AdminWindow::~AdminWindow()
{
    delete ui;
}

void AdminWindow::on_psearch_clicked()
{
    if(bc==_region){model->fregion(ui->psearchl->text());}
    else if(bc==_pred){model->fpred(ui->psearchl->text());}
    else if(bc==_jurnal){model->jurnal(ui->psearchl->text());}

}

void AdminWindow::on_preset_clicked()
{
    model->resetFilter();
}


void AdminWindow::on_ptable_doubleClicked(const QModelIndex &index)
{
    User *p = model->_data(sortmodel->mapToSource(index).row());
    addchange ch(this, p);
    ch.exec();
}

void AdminWindow::on_newp_released()
{
    addchange ch(this, nullptr);
    ch.exec();
}

void AdminWindow::on_pcomboBox_currentIndexChanged(const QString &arg1)
{
    bc = arg1;
}

void AdminWindow::on_btnEditPatient_released()
{
    QModelIndexList indices = ui->ptable->selectionModel()->selectedRows();
    if (indices.size() != 1) {
        QMessageBox::warning(this, "Предупреждение", "колонку "
                             ". Use ");
        return;
    }
    QModelIndex index = indices.at(0);
    User *p = model->_data(sortmodel->mapToSource(index).row());
    int adminID = Client::instance()->getID();
    int userID = p->id();
    jurnal *j = new jurnal(nullptr);
    j->loadList(adminID, userID);
    j->show();
}

void AdminWindow::on_btnReload_clicked()
{
    loadList();
}

void AdminWindow::on_newp_clicked()
{

}
