#ifndef USERTABLEVIEV_H
#define USERTABLEVIEV_H


#include <vector>
#include "user.h"
#include <QAbstractTableModel>

using namespace std;


class  UserTableView : public QAbstractTableModel
{
public:
    UserTableView(vector< User *> *users);
    int rowCount (const QModelIndex &parent = QModelIndex()) const;
    int columnCount (const QModelIndex &parent = QModelIndex()) const;
    QVariant data (const QModelIndex & index, int role = Qt::DisplayRole) const;
     User *_data(int row);
    QVariant headerData(int section, Qt::Orientation orientation,int role = Qt::DisplayRole) const;
    void beginResetModel_();
    void endResetModel_();
    void fregion(const QString region);
    void fpred(const QString pred);
    void jurnal(const QString jun);
    void resetFilter();
    void refresh();


private:
    //patientpass *pp;
    vector<unsigned int> positions;
    bool isFilter = false;
    vector<User *> *_users;
};

#endif // USERTABLEVIEV_H
