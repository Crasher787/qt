#ifndef JURNAL_H
#define JURNAL_H

#include <QWidget>
#include <vector>
#include <QSortFilterProxyModel>
#include "jurnaltableview.h"
#include "jurnalitem.h"

using namespace std;

namespace Ui {
class jurnal;
}


class jurnal : public QWidget
{
    Q_OBJECT

public:
    explicit jurnal(QWidget *parent = nullptr);
    ~jurnal();

    void loadList(int adminID, int userID);

private:
    Ui::jurnal *ui;
    int _adminID;
    int _userID;
    JurnalTableView* model;
    QSortFilterProxyModel* sortmodel;
    vector<JurnalItem *> _items;
    void clearJournal();
private slots:
    void processJson(const QJsonDocument &json);
    void on_btnAdd_clicked();
};

#endif // JURNAL_H
