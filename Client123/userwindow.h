#ifndef USERWINDOW_H
#define USERWINDOW_H

#include <QTableView>
#include "admintableview.h"
#include <QWidget>
#include <QtGui>
#include <vector>
#include <user.h>
#include "admin.h"

using namespace std;

namespace Ui {
    class UserWindow;
}


class UserWindow : public QWidget
{
    Q_OBJECT

public:
    explicit UserWindow(QWidget *parent = nullptr);
    ~UserWindow();
    void loadList();

private slots:
    void on_dsearch_clicked();

    void on_dreset_clicked();

    void processJson(const QJsonDocument &json);

    void on_comboBox_currentIndexChanged(const QString &arg1);

    void on_table_doubleClicked(const QModelIndex &index);

    void on_btnReload_clicked();

private:
    Ui::UserWindow *ui;
    AdminTAbleView* model;
    QSortFilterProxyModel* sortmodel;
    QString bc;
    QString bd;
    QString _telp = "Регион";
    QString _fiop = "Предприятия";
    QString _painp = "Показатели";
    QString _painp1="Учет";
    QStringList parameters;
    vector<admin *> _admins;
    void clearadmin();
};

#endif // USERWINDOW_H
