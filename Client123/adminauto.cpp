#include "adminauto.h"
#include "ui_adminauto.h"
#include <QtGui>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QApplication>
#include "client.h"

Adminauto::Adminauto(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Adminauto)
{
    ui->setupUi(this);
    adminWindow = new AdminWindow();
    this->setWindowTitle("Админ");
    connect(Client::instance(), SIGNAL(jsonRetrieved(const QJsonDocument &)),
            this, SLOT(processJson(const QJsonDocument &)));
}

Adminauto::~Adminauto()
{
    delete ui;
}

void Adminauto::processJson(const QJsonDocument &json) {
    qInfo() << "year!\n";
    QJsonObject o = json.object();
    if (o["action"].toString() != "admin_auth") {
        return;
    }
    qInfo() << "year action: " << o["action"].toString() << "\n";
    if (o["success"].toBool()) {
        Client::instance()->setID(o["doctor"].toObject()["id"].toInt());
        adminWindow->loadList();
        QMessageBox::information(this, "Привет!", "Добро пожаловать, doctor!");
        this->close();
        adminWindow->show();
    } else {
        QMessageBox::warning(this, "Пока!", "ТЫ кто!");
        _counter++;
        if (_counter >= 3) {
            QMessageBox::warning(this, "No!", "Пока!");
            QApplication::instance()->quit();
        }
    }
    /*
    json.
    storage &stor = storage::instance();
    const doctorpass *pm = stor.dpass();
    const QString &pass=pm->password(login);
    if(!pass.toStdString().empty())
    {
    }else
    {
        QMessageBox::warning(this, "No!", "I don't know you!");
    }
    */
}

void Adminauto::on_Ok_clicked()
{
    QString login = ui->login_2->text();
    QString pass = ui->password_2->text();
    QJsonObject obj;
    obj.insert("action", "admin_auth");
    obj.insert("login", login);
    obj.insert("pas", pass);
    QJsonDocument authJson(obj);
    Client::instance()->send(authJson);
}

void Adminauto::on_quit_clicked()
{
    QApplication::quit();
}
