#ifndef USERAUTO_H
#define USERAUTO_H

#include <QDialog>
#include "userwindow.h"

namespace Ui {
class  UserAuto;
}

class  UserAuto : public QDialog
{
    Q_OBJECT

public:
    explicit  UserAuto(QWidget *parent = nullptr);
    ~ UserAuto();

private slots:
    void on_Ok_clicked();
    void processJson(const QJsonDocument &json);
    void on_quit_clicked();


private:
    Ui:: UserAuto *ui;
    //const patientpass &pm;
    //const doctorpass *pp;
    UserWindow *userWindow;
    int _counter = 0;
};


#endif // USERAUTO_H
