#include "client.h"
#include <QTcpSocket>
#include <QTextEdit>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QLabel>
#include <QTime>
#include <QTextCodec>
#include <QJsonDocument>
#include <QtDebug>
#include "encriptor.h"

Client *Client::instance() {
    static Client *inst = nullptr;
    if (inst == nullptr) {
        inst = new Client();
    }
    return inst;
}

Client::Client() {
    _nextBlockSize = 0;
}

void Client::connect(const QString &strHost, quint16 port) {
    _tcpSocket = new QTcpSocket(this);
    _tcpSocket->connectToHost(strHost, port);


    //QObject::connect(_tcpSocket, SIGNAL(connected()), this, SLOT(slotConnected()));
    QObject::connect(_tcpSocket, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));

}

void Client::slotReadyRead(){
    while (true) {
        QDataStream in(_tcpSocket);
        //in.setVersion(QDataStream::Qt_5_10);
        qInfo() << "started0 " << _tcpSocket->bytesAvailable() << " " << _nextBlockSize << "\n";

        if (_nextBlockSize == 0){
            if (_tcpSocket->bytesAvailable() < static_cast<int>(sizeof(quint16))){
                return;
            }
            in >> _nextBlockSize;
        }
        qInfo() << "started2 " << _tcpSocket->bytesAvailable() << " " << _nextBlockSize << "\n";

        if (_tcpSocket->bytesAvailable() < _nextBlockSize){
            return;
        }
        qInfo() << "started! " << _tcpSocket->bytesAvailable() << "\n";

        //QTime time;
        QByteArray inputBytes;
        in >> inputBytes;
        _nextBlockSize = 0;
        QJsonDocument json;
        if (!Encriptor::instance()->decrypt(inputBytes, json)) {
            qWarning() << "Failed to decrypt message from server\n";
            return;
        }
        qInfo() << "json: " << json << "\n";
        emit jsonRetrieved(json);
    }
}

void Client::send(const QJsonDocument &json){
    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    //out.setVersion(QDataStream::Qt_5_10);
    //QByteArray ar = QTextCodec::codecForMib(106)->fromUnicode(_textInput->text());

    QByteArray encrypted;
    if (!Encriptor::instance()->encrypt(json, encrypted)) {
        qWarning() << "Failed to encrypt message\n";
        return;
    }

    out << quint16(encrypted.size() - static_cast<int>(sizeof(quint16))) << encrypted;

    _tcpSocket->write(arrBlock);
}

/*
void Client::slotConnected(){
    //_textInfo->append("Received the \"connected\" signal.");
}
*/

void Client::setID(int id) {
    _id = id;
}

int Client::getID() {
    return _id;
}
