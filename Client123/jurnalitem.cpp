#include "jurnalitem.h"
#include <QJsonObject>
#include <QDateTime>

JurnalItem::JurnalItem(int id, int adminID, int userID, QDateTime when)
{
   _id = id;
   _adminID = adminID;
   _userID = userID;
   _when = when;
}

JurnalItem::JurnalItem(const QJsonObject &json) {
    _id = json["id"].toInt();
    _adminID = json["id_admin"].toInt();
    _userID = json["id_user"].toInt();
    QString w = json["when"].toString();
    _when = QDateTime::fromString(w, Qt::ISODate);
}

void JurnalItem::toJson(QJsonObject &res) {
    res.insert("id", _id);
    res.insert("id_admin", _adminID);
    res.insert("id_user", _userID);
    res.insert("when", _when.toString(Qt::ISODate));
}

