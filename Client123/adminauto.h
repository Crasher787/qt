#ifndef ADMINAUTO_H
#define ADMINAUTO_H
#include "adminwindow.h"
#include <QDialog>

namespace Ui {
class Adminauto;
}

class Adminauto : public QDialog
{
    Q_OBJECT

public:
    explicit Adminauto(QWidget *parent = nullptr);
    ~Adminauto();

private slots:
    void on_Ok_clicked();
    void processJson(const QJsonDocument &json);
    void on_quit_clicked();

private:
    Ui::Adminauto *ui;
    AdminWindow *adminWindow;
      int _counter = 0;
};

#endif // ADMINAUTO_H
