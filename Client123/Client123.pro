#-------------------------------------------------
#
# Project created by QtCreator 2019-06-10T02:46:23
#
#-------------------------------------------------

QT       += core gui

QT += network
QT += sql

INCLUDEPATH += D:/openssl_git/include
LIBS += -LD:/openssl_git/
LIBS += -LD:/openssl_git/lib -llibssl
LIBS += -LD:/openssl_git/lib -llibcrypto


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Client123
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        addchange.cpp \
        admin.cpp \
        adminauto.cpp \
        admintableview.cpp \
        adminwindow.cpp \
        choice.cpp \
        client.cpp \
        encriptor.cpp \
        jurnal.cpp \
        jurnalitem.cpp \
        jurnaltableview.cpp \
        main.cpp \
        mainwindow.cpp \
        user.cpp \
        userauto.cpp \
        usertableviev.cpp \
        userwindow.cpp

HEADERS += \
        addchange.h \
        admin.h \
        adminauto.h \
        admintableview.h \
        adminwindow.h \
        choice.h \
        client.h \
        encriptor.h \
        jurnal.h \
        jurnalitem.h \
        jurnaltableview.h \
        mainwindow.h \
        user.h \
        userauto.h \
        usertableviev.h \
        userwindow.h

FORMS += \
        addchange.ui \
        adminauto.ui \
        adminwindow.ui \
        choice.ui \
        jurnal.ui \
        mainwindow.ui \
        userauto.ui \
        userwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
