#include "user.h"
#include <QJsonObject>

User::User(int id, QString login, QString pas, QString region, QString pred,
           QString nampred, QString prod, QString jurnal)
{
    _id = id;
    _login = login;
    _pas = pas;
    _region = region;
    _pred = pred;
    _nampred = nampred;
    _prod = prod;
    _jurnal = jurnal;
}

User::User(const QJsonObject &json) {
    _id = json["id"].toInt();
    _login = json["login"].toString();
    _pas = json["pas"].toString();
    _region = json["region"].toString();
    _pred = json["pred"].toString();
    _nampred = json["nampred"].toString();
    _prod = json["prod"].toString();
    _jurnal = json["jurnal"].toString();
}

void User::toJson(QJsonObject &res) {
    res.insert("id", _id);
    res.insert("login", _login);
    res.insert("pas", _pas);
    res.insert("region", _region);
    res.insert("pred", _pred);
    res.insert("nampred", _nampred);
    res.insert("prod", _prod);
    res.insert("jurnal", _jurnal);
}
