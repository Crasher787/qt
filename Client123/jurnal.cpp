#include "jurnal.h"
#include "client.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QSortFilterProxyModel>
#include "jurnaltableview.h"
#include "jurnalitem.h"

jurnal::jurnal(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::jurnal)
{
    ui->setupUi(this);
    model = new JurnalTableView(&_items);
    sortmodel = new QSortFilterProxyModel();
    sortmodel->setSourceModel(model);
    ui->jurnalTable->setModel(sortmodel);
    ui->jurnalTable->setSortingEnabled(true);
    this->setWindowTitle("Журнал посещений");
    connect(Client::instance(), SIGNAL(jsonRetrieved(const QJsonDocument &)),
            this, SLOT(processJson(const QJsonDocument &)));
}

jurnal::~jurnal()
{
    delete ui;
}

void jurnal::loadList(int adminID, int userID) {
    _adminID = adminID;
    _userID = userID;
    QJsonObject obj;
    obj.insert("action", "list_journal");
    obj.insert("id_admin", adminID);
    obj.insert("id_user", userID);
    QJsonDocument json(obj);
    Client::instance()->send(json);
}

void jurnal::processJson(const QJsonDocument &json) {
    model->beginResetModel_();
    QJsonObject o = json.object();
    if (o["action"].toString() != "list_journal") {
        return;
    }
    QJsonArray a = o["items"].toArray();
    clearJournal();
    for (const QJsonValue &v : a) {
        QJsonObject jsonItem = v.toObject();
        _items.push_back(new JurnalItem(jsonItem));
    }
    model->endResetModel_();
}

void jurnal::clearJournal() {
    for (JurnalItem *v : _items) {
        delete v;
    }
    _items.clear();
}

void jurnal::on_btnAdd_clicked()
{
    QJsonObject obj;
    obj.insert("action", "add_journal_item");
    obj.insert("id_admin", _adminID);
    obj.insert("id_user", _userID);
    QDateTime when = ui->dateTimeEdit->dateTime();
    JurnalItem item(-1, _adminID, _userID, when);
    QJsonObject itemObj;
    item.toJson(itemObj);
    obj.insert("item", itemObj);
    QJsonDocument json(obj);
    Client::instance()->send(json);
}
