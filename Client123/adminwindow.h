#ifndef ADMINWINDOW_H
#define ADMINWINDOW_H

#include <QTableView>
#include "usertableviev.h"
#include <QWidget>
#include <QtGui>
#include <QSortFilterProxyModel>
#include <vector>
#include <QDialog>
#include "user.h"

using namespace std;

namespace Ui {
    class AdminWindow;
}


class AdminWindow : public QWidget
{
    Q_OBJECT

public:
    explicit AdminWindow(QWidget *parent = nullptr);
    ~AdminWindow();
    void loadList();

private slots:
    void on_psearch_clicked();

    void on_preset_clicked();

    void on_ptable_doubleClicked(const QModelIndex &index);

    void on_newp_released();

    void processJson(const QJsonDocument &json);

    void on_pcomboBox_currentIndexChanged(const QString &arg1);

    void on_btnEditPatient_released();

    void on_btnReload_clicked();

    void on_newp_clicked();

private:
    Ui::AdminWindow *ui;
    //QTableView* view;
    UserTableView* model;
    QSortFilterProxyModel* sortmodel;
    //QStringList parameters;
    QString bc;
    QString bd;
    QString _region = "Регион";
    QString _pred = "Предприятия";
    QString _jurnal = "Показатели";
    QString _painp1="Учет";
    QStringList user;
    vector<User *> _users;
    void clearPatients();
};


#endif // ADMINWINDOW_H
