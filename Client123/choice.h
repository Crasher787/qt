#ifndef CHOICE_H
#define CHOICE_H

#include <QMainWindow>
#include "adminauto.h"
#include "userauto.h"
#include "addchange.h"


namespace Ui {
class Choice;
}

class Choice : public QMainWindow
{
    Q_OBJECT

public:
    explicit Choice(QWidget *parent = nullptr);
    ~Choice();

private slots:

    void on_quit_clicked();

    void on_pushButton_clicked();

private:
    Ui::Choice *ui;
   Adminauto *AdminAuth;
    UserAuto *UserAuth;
    addchange *sada;
};;

#endif // CHOICE_H
