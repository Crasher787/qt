#include "userauto.h"
#include "ui_userauto.h"
#include <QtGui>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QApplication>
#include "client.h"
#include <QWidget>
#include <QtDebug>

UserAuto::UserAuto(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UserAuto)
{
    ui->setupUi(this);
    userWindow = new UserWindow(nullptr);
    this->setWindowTitle("Авторизация Юзера");
    connect(Client::instance(), SIGNAL(jsonRetrieved(const QJsonDocument &)),
            this, SLOT(processJson(const QJsonDocument &)));
}

UserAuto::~UserAuto()
{
    delete ui;
}

void UserAuto::processJson(const QJsonDocument &json) {
    QJsonObject o = json.object();
    if (o["action"].toString() != "user_auth") {
        return;
    }
    if (o["success"].toBool()) {
        Client::instance()->setID(o["user"].toObject()["id"].toInt());
        userWindow->loadList();
        QMessageBox::information(this, "Yes!", "I know you, user!");
        this->close();
        userWindow->show();
    } else {
        QMessageBox::warning(this, "No!", "I don't know you!");
        _counter++;
        if (_counter >= 3) {
            QMessageBox::warning(this, "No!", "Buy-buy!");
            QApplication::instance()->quit();
        }
    }
    /*
    json.
    storage &stor = storage::instance();
    const doctorpass *pm = stor.dpass();
    const QString &pass=pm->password(login);
    if(!pass.toStdString().empty())
    {
    }else
    {
        QMessageBox::warning(this, "No!", "I don't know you!");
    }
    */
}

void UserAuto::on_Ok_clicked()
{
    QString login = ui->login->text();
    QString pass = ui->password->text();
    QJsonObject obj;
    obj.insert("action", "user_auth");
    obj.insert("login", login);
    obj.insert("pas", pass);
    QJsonDocument authJson(obj);
    Client::instance()->send(authJson);
}

void UserAuto::on_quit_clicked()
{
    QApplication::quit();
}
