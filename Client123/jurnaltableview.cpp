#include "jurnaltableview.h"
#include <QtDebug>
#include "jurnalitem.h"

JurnalTableView::JurnalTableView(vector<JurnalItem *> *items){
    _items = items;
}

int JurnalTableView::rowCount(const QModelIndex &parent)const{
    return _items->size();
}


int JurnalTableView::columnCount (const QModelIndex &parent) const{
    return 1;
}

void JurnalTableView::beginResetModel_() {
    beginResetModel();
}

void JurnalTableView::endResetModel_() {
    endResetModel();
}

QVariant JurnalTableView::data (const QModelIndex & index, int role) const{
    if (role == Qt::DisplayRole) {
        JurnalItem *item = _items->at(index.row());
        switch (index.column()) {
        case 0:
            return item->when();
        default:
            return QVariant();
        }
    } else if (role == Qt::TextAlignmentRole){
        if (index.column() == 0)
            return QVariant(Qt::AlignLeft | Qt::AlignVCenter);
        else
            return Qt::AlignCenter;
    }
    return QVariant();
}

QVariant JurnalTableView::headerData(int section, Qt::Orientation orientation,int role) const{
    if (role != Qt::DisplayRole)
           return QVariant();

       if (orientation == Qt::Horizontal) {
           switch (section) {
           case 0:
               return tr("When");
           default:
               return QVariant();
           }
       }
       return section + 1;
}
