#ifndef ADDCHANGE_H
#define ADDCHANGE_H
#include <QDialog>
#include <user.h>
namespace Ui {
class addchange;
}


class addchange : public QDialog
{
    Q_OBJECT

public:
    explicit addchange(QWidget *parent = nullptr, User *p = nullptr);
    ~addchange();

private slots:

    void on_submit_released();

    void on_cancel_released();

    void on_change_accepted();

private:
    Ui::addchange *ui;
    int _userID;
};

#endif // ADDCHANGE_H
