#ifndef CLIENT_H
#define CLIENT_H

#include <QWidget>
#include <QTcpSocket>

class QTextEdit;
class QLineEdit;

class Client: public QObject{
    Q_OBJECT

public:
    void connect(const QString &strHost, quint16 port);
    static Client *instance();
    void setID(int id);
    int getID();

private:
    Client();
    QTcpSocket *_tcpSocket;
    quint16 _nextBlockSize;
    int _id = -1;
signals:
    void jsonRetrieved(const QJsonDocument &json);
private slots:
    void slotReadyRead();
public slots:
    //void slotError(QAbstractSocket::SocketError);
    void send(const QJsonDocument &json);
    //void slotConnected();
};

#endif // CLIENT_H
