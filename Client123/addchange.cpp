#include "addchange.h"
#include "ui_addchange.h"
#include <QMessageBox>
#include "user.h"
#include "client.h"
#include <QJsonObject>
#include <QJsonDocument>

addchange::addchange(QWidget *parent, User* p) :
    QDialog(parent),
    ui(new Ui::addchange)
{
    ui->setupUi(this);

    this->setWindowTitle("Добвление информации");

    if (p == nullptr) {
        _userID = -1;
    } else {
        _userID = p->id();
    }
    ui->lineid->setText(QString::number(_userID));
    ui->lineid->setReadOnly(true);
    if (p != nullptr) {
        ui->linelogin->setText(p->login());
        ui->linepassword->setText(p->pas());
        ui->linesurname->setText(p->region());
        ui->linename->setText(p->pred());
        ui->linesecondname->setText(p->nampred());
        ui->linetel->setText(p->prod());
        ui->linepain->setText(p->jurnal());
    }

}

addchange::~addchange()
{
    delete ui;
}

void addchange::on_submit_released()
{
    if(ui->linelogin->text()=="" || ui->linepassword->text()=="" || ui->linetel->text()=="" || ui->linesurname->text()=="" || ui->linename->text()=="" || ui->linesecondname->text()=="" || ui->linepain->text()==""){QMessageBox::warning(this, "not yet", "you have empty line(s)");}
    else if (ui->linesurname->text().at(0)<"А" || ui->linesurname->text().at(0)>"Я") {QMessageBox::warning(this, "Наа", "С Заглавной буквы");}
    else if (ui->linesurname->text().at(0)<"А" || ui->linename->text().at(0)>"Я") {QMessageBox::warning(this, "Наа", "С Заглавной буквы");}
    else if (ui->linesurname->text().at(0)<"А" || ui->linesecondname->text().at(0)>"Я") {QMessageBox::warning(this, "Наа ", "С Заглавной буквы");}
      else {
            accept();
        }
    }

void addchange::on_cancel_released()
{
    reject();
}

void addchange::on_change_accepted()
{
    QJsonObject json;
    if (_userID == -1) {
        json.insert("action", "add_patient");
    } else {
        json.insert("action", "update_patient");
    }
    json.insert("ID", ui->lineid->text().toInt());
   json.insert("login", ui->linelogin->text());
   json.insert("pas", ui->linepassword->text());
    json.insert("region", ui->linetel->text());
    json.insert("pred", ui->linesurname->text());
    json.insert("nampred", ui->linename->text());
    json.insert("prod", ui->linesecondname->text());
    json.insert("jurnal", ui->linepain->text());
    QJsonDocument doc(json);
    Client::instance()->send(doc);
}
