#include "admintableview.h"
#include <QtDebug>

#include "admin.h"

AdminTAbleView::AdminTAbleView(vector<admin *> *admins){
    _admins= admins;
}

int AdminTAbleView::rowCount(const QModelIndex &parent)const{
    if (this->isFilter) {
        return positions.size();
    }
    return _admins->size();
}


int AdminTAbleView::columnCount (const QModelIndex &parent) const{
    return 3;
}

void AdminTAbleView::beginResetModel_() {
    beginResetModel();
}

void AdminTAbleView::endResetModel_() {
    endResetModel();
}

QVariant AdminTAbleView::data (const QModelIndex & index, int role) const{
    if (role == Qt::DisplayRole) {
        admin *d;
        if (isFilter) {
            unsigned int ind = positions.at(index.row());
            d = _admins->at(ind);
        } else {
            d = _admins->at(index.row());
        }
        switch (index.column()) {
        case 0:
            return d->region();
        case 1:
            return d->pred() + " " + d->nampred() + " " + d->prod();
        case 2:
            return d->jurnal();
        case 3:
            return d->jurnal();
        default:
            return QVariant();
        }
    } else if (role == Qt::TextAlignmentRole){
        if (index.column() == 0)
            return QVariant(Qt::AlignLeft | Qt::AlignVCenter);
        else
            return Qt::AlignCenter;
    }
    return QVariant();
}




QVariant AdminTAbleView::headerData(int section, Qt::Orientation orientation,int role) const{
    if (role != Qt::DisplayRole)
           return QVariant();

       if (orientation == Qt::Horizontal) {
           switch (section) {
           case 0:
               return tr("Регион");
           case 1:
               return tr("Предприятия");
           case 2:
               return tr("Показатели");
           case 3:
               return tr("Учет");
           default:
               return QVariant();
           }
       }
       return section + 1;
}

void AdminTAbleView::fregion(const QString region)
{
    beginResetModel();
    isFilter=true;
    positions.clear();
    for (unsigned i = 0; i < _admins->size(); i++) {
        if (_admins->at(i)->region().contains(region)) {
            positions.push_back(i);
        }
    }
    endResetModel();
}

void AdminTAbleView::fpred(const QString pred)
{
    beginResetModel();
    isFilter=true;
    positions.clear();
    for (unsigned i = 0; i < _admins->size(); i++) {
        admin *d = _admins->at(i);
        if ((d->pred() + " " + d->nampred() + " " + d->prod()).contains(pred)) {
            positions.push_back(i);
        }
    }
    endResetModel();
}

void AdminTAbleView::jurnal(const QString jun)
{
    beginResetModel();
    isFilter=true;
    positions.clear();
    for (unsigned i = 0; i < _admins->size(); i++) {
        if (_admins->at(i)->jurnal().contains(jun)) {
            positions.push_back(i);
        }
    }
    endResetModel();
}




void AdminTAbleView::resetFilter()
{
    beginResetModel();
    isFilter=false;
    endResetModel();
}

admin* AdminTAbleView::_data(int row)
{
    admin *d;
    if (isFilter) {
        unsigned int ind = positions.at(row);
        d = _admins->at(ind);
    } else {
        d = _admins->at(row);
    }
    return d;
}
