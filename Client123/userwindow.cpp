#include "userwindow.h"
#include "ui_userwindow.h"
#include <QtGui>
#include <QMessageBox>
#include "client.h"
#include "admin.h"
#include "jurnal.h"

UserWindow::UserWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserWindow)
{
    ui->setupUi(this);
    model = new AdminTAbleView(&_admins);
    sortmodel = new QSortFilterProxyModel();
    sortmodel->setSourceModel(model);
    ui->dtable->setModel(sortmodel);
    ui->dtable->setSortingEnabled(true);
    parameters<<_telp<<_fiop<<_painp<<_painp1;
    ui->dcomboBox->addItems(parameters);
    this->setWindowTitle("USer");
    connect(Client::instance(), SIGNAL(jsonRetrieved(const QJsonDocument &)),
            this, SLOT(processJson(const QJsonDocument &)));
}

UserWindow::~UserWindow()
{
    delete ui;
}

void UserWindow::on_dsearch_clicked()
{
    if(bc==_region){model->fregion(ui->psearchl->text());}
    else if(bc==_pred){model->fpred(ui->psearchl->text());}
    else if(bc==_jurnal){model->jurnal(ui->psearchl->text());}
}


void UserWindow::on_dreset_clicked()
{
    model->resetFilter();
}

void UserWindow::loadList() {
    QJsonObject obj;
    obj.insert("action", "list_admin");
    obj.insert("my_id", Client::instance()->getID());
    QJsonDocument json(obj);
    Client::instance()->send(json);
}

void UserWindow::processJson(const QJsonDocument &json) {
    model->beginResetModel_();
    QJsonObject o = json.object();
    if (o["action"] != "list_admin") {
        return;
    }
    QJsonArray a = o["items"].toArray();
    clearadmin();
    for (const QJsonValue &v : a) {
        QJsonObject jsonItem = v.toObject();
        _admins.push_back(new admin(jsonItem));
    }
    model->endResetModel_();
    ui->dtable->resizeColumnsToContents();
}

void UserWindow::clearadmin() {
    for (admin *v : _admins) {
        delete v;
    }
    _admins.clear();
}

void UserWindow::on_comboBox_currentIndexChanged(const QString &arg1)
{
    bc = arg1;
}

void UserWindow::on_table_doubleClicked(const QModelIndex &index)
{
    admin *d = model->_data(sortmodel->mapToSource(index).row());
    int doctorID = d->id();
    int patientID = Client::instance()->getID();
    jurnal *j = new jurnal(nullptr);
    j->loadList(doctorID, patientID);
    j->show();
}

void UserWindow::on_btnReload_clicked()
{
    loadList();
}
