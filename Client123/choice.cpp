#include "choice.h"
#include "ui_choice.h"


Choice::Choice(QWidget *parent) :

    QMainWindow(parent),

    ui(new Ui::Choice)
{
    ui->setupUi(this);

    AdminAuth = new Adminauto();
    UserAuth = new UserAuto();
    sada = new addchange();
    this->setWindowTitle("Выберай!");
}

Choice::~Choice()
{
    delete ui;
}


void Choice::on_quit_clicked()
{
    QApplication::quit();
}

void Choice::on_pushButton_clicked()
{

    if(ui->checkBox->isChecked())
    {
        this->close();
        AdminAuth->show();
    }
    if(ui->checkBox_2->isChecked())
    {
        this->close();
    UserAuth->show();
    }
}
