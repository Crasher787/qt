#include "usertableviev.h"
#include <QMessageBox>
#include <QtDebug>
#include "user.h"

UserTableView::UserTableView(vector<User *> *users){
    _users = users;
}

void UserTableView::beginResetModel_() {
    beginResetModel();
}

void UserTableView::endResetModel_() {
    endResetModel();
}

int UserTableView::rowCount(const QModelIndex &parent)const{
    if (this->isFilter) {
        return positions.size();
    }
    return _users->size();
}


int UserTableView::columnCount (const QModelIndex &parent) const{
    return 3;
}

QVariant UserTableView::data (const QModelIndex & index, int role) const{
    if (role == Qt::DisplayRole) {
        User *p;
        if (isFilter) {
            unsigned int ind = positions.at(index.row());
            p = _users->at(ind);
        } else {
            p = _users->at(index.row());
        }
        switch (index.column()) {
        case 0:
            return p->region();
        case 1:
            return p->pred() + " " + p->nampred() + " " + p->prod();
        case 2:
            return p->jurnal();
        case 3:
            return p->jurnal();
        default:
            return QVariant();
        }
    }
    else if (role == Qt::TextAlignmentRole){
        if (index.column() == 0)
            return QVariant(Qt::AlignLeft | Qt::AlignVCenter);
        else
            return Qt::AlignCenter;
    }
    return QVariant();
}

User* UserTableView::_data(int row)
{
    User *p;
    if (isFilter) {
        unsigned int ind = positions.at(row);
        p = _users->at(ind);
    } else {
        p = _users->at(row);
    }
    return p;
}

void UserTableView::fregion(const QString region)
{
    beginResetModel();
    isFilter=true;
    positions.clear();
    for (unsigned i = 0; i < _users->size(); i++) {
        if (_users->at(i)->region().contains(region)) {
            positions.push_back(i);
        }
    }
    endResetModel();
}

void UserTableView::fpred(const QString pred)
{
    beginResetModel();
    isFilter=true;
    positions.clear();
    for (unsigned i = 0; i < _users->size(); i++) {
        User *p = _users->at(i);
        if ((p->pred() + " " + p->nampred() + " " + p->prod()).contains(pred)) {
            positions.push_back(i);
        }
    }
    endResetModel();
}

void UserTableView::jurnal(const QString jun)
{
    beginResetModel();
    isFilter=true;
    positions.clear();
    for (unsigned i = 0; i < _users->size(); i++) {
        if (ui->at(i)->jurnal().contains(jun)) {
            positions.push_back(i);
        }
    }
    endResetModel();
}



void UserTableView::resetFilter()
{
    beginResetModel();
    isFilter=false;
    endResetModel();
}

void UserTableView::refresh()
{
    beginResetModel();
    endResetModel();
}

QVariant UserTableView::headerData(int section, Qt::Orientation orientation,int role) const{
    if (role != Qt::DisplayRole)
           return QVariant();

       if (orientation == Qt::Horizontal) {
           switch (section) {
           case 0:
               return tr("Регион");
           case 1:
               return tr("Предприятия");
           case 2:
               return tr("Показатели");
           case 3:
               return tr("Учет");
           default:
               return QVariant();
           }
       }
       return section + 1;
}
