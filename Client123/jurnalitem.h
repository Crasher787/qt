#ifndef JURNALITEM_H
#define JURNALITEM_H

#include <QString>
#include <QDateTime>
#include <QJsonObject>


class JurnalItem
{
private:
    int _id;
    int _adminID;
    int _userID;
    QDateTime _when;
public:
    JurnalItem() {}
    JurnalItem(int id, int adminID, int userID, QDateTime when);
    JurnalItem(const QJsonObject &json);
    int id() const { return _id; }
    int adminID() const { return _adminID; }
    int userID() const { return _userID; }
    QDateTime when() const { return _when; }
    void id(int value) { _id = value; }
    void adminID(int value) { _adminID = value; }
    void userID(int value) { _userID = value; }
    void when(QDateTime value) { _when = value; }
    void toJson(QJsonObject &res);
};


#endif // JURNALITEM_H
