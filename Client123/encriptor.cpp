#include "encriptor.h"
#include <string.h>
#include <openssl/conf.h>
#include <openssl/conf.h>
#include <openssl/err.h>
#include <openssl/aes.h>
#include <openssl/evp.h>
#include <QByteArray>
#include <QJsonDocument>
#include <QtDebug>

Encriptor::Encriptor(const char *key, const char *iv)
{
    this->key = key;
    this->iv = iv;
}

Encriptor *Encriptor::instance() {
    static Encriptor *inst = nullptr;
    if (inst == nullptr) {
        inst = new Encriptor("0123456789abcdeF0123456789abcdeF", "1234567887654321");
    }
    return inst;
}

int Encriptor::doCrypt(const QByteArray &in, QByteArray &out, int doEncrypt){
    /* Allow enough space in output buffer for additional block */
    unsigned char inbuf[1024], outbuf[1024 + EVP_MAX_BLOCK_LENGTH];
    int inlen, outlen;

    out.clear();

    EVP_CIPHER_CTX *ctx;
    ctx = EVP_CIPHER_CTX_new();

    /* Don't set key or IV right away; we want to check lengths */
    EVP_CipherInit_ex(ctx, EVP_aes_256_cbc(), NULL, NULL, NULL, doEncrypt);
    OPENSSL_assert(EVP_CIPHER_CTX_key_length(ctx) == 32);
    OPENSSL_assert(EVP_CIPHER_CTX_iv_length(ctx) == 16);

    /* Now we can set key and IV */
    EVP_CipherInit_ex(ctx, NULL, NULL, (unsigned char *)key, (unsigned char *)iv, doEncrypt);

    int countRead = 0;
    int len = in.length();
    for(;;){
        inlen = 1024;
        if (len - countRead < 1024) {
            inlen = len - countRead;
        }
        memcpy(inbuf, in.constData() + countRead, inlen);
        countRead += inlen;
        if (inlen <= 0) break;
        if(!EVP_CipherUpdate(ctx, outbuf, &outlen, inbuf, inlen)){
            /* Error */
            EVP_CIPHER_CTX_free(ctx);
            return 0;
        }
        if (outlen == 0) {
            continue;
        }
        QByteArray outPart((const char *)outbuf, outlen);
        out.append(outPart);
    }

    if(!EVP_CipherFinal_ex(ctx, outbuf, &outlen)){
        /* Error */
        EVP_CIPHER_CTX_free(ctx);
        return 0;
    }
    QByteArray outPart((const char *)outbuf, outlen);
    out.append(outPart);
    EVP_CIPHER_CTX_free(ctx);
    return 1;
}

bool Encriptor::encrypt(const QJsonDocument &inputJson, QByteArray &encrypted) {
    QByteArray input = inputJson.toJson();
    int res = doCrypt(input, encrypted, 1);
    return res == 1;
}

bool Encriptor::decrypt(const QByteArray &inputBytes, QJsonDocument &encrypted) {
    QByteArray output;
    int res = doCrypt(inputBytes, output, 0);
    encrypted = QJsonDocument::fromJson(output);
    return res == 1;
}

