#ifndef ENCRIPTOR_H
#define ENCRIPTOR_H


class QJsonDocument;
class QByteArray;

class Encriptor
{
public:
    static Encriptor *instance();
    bool encrypt(const QJsonDocument &inputJson, QByteArray &encrypted);
    bool decrypt(const QByteArray &inputBytes, QJsonDocument &encrypted);
private:
    Encriptor(const char *key, const char *iv);

    const char *key;
    const char *iv;

    int doCrypt(const QByteArray &in, QByteArray &out, int doEncrypt);
};
#endif // ENCRIPTOR_H
