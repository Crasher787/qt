#include <QCoreApplication>
#include <QtSql>
#include <QDebug>
#include <QSqlDatabase>


static bool createConnection()
{
QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
db.setDatabaseName("addressbook"); //название базы данных, можно указать путь
if (!db.open())
{ // открытие базы данных
qDebug() << "Cannot open database:" << db.lastError().text(); // предупреждение об ошибки
return false;
}
return true;
}

int main(int argc, char** argv)
{
setlocale(LC_ALL, "ru");

QCoreApplication app(argc, argv);
// Соединяемся с менеджером баз данных
if (!createConnection())
{
return -1;
}

// Создаем базу
QSqlQuery query;
QString str = "CREATE TABLE addressbook ( number INTEGER AUTOINCREMENT NOT NULL,name VARCHAR(15),phone VARCHAR(12),email VARCHAR(15))";
if (!query.exec(str))
{
qDebug() << "Unable to create a table";
}

// Добавляем данные в базу
QString strF =
"INSERT INTO addressbook (number, name, phone, email)"
"VALUES(%1, '%2', '%3', '%4');";

str = strF.arg("1")
.arg("Piggy")
.arg("+49 631322187")
.arg("piggy@mega.de");
if (!query.exec(str))
{
qDebug() << "Unable to make insert operation";
}
str = strF.arg("2")
.arg("Kermit")
.arg("+49 631322181")
.arg("kermit@mega.de");
if (!query.exec(str))
{
qDebug() << "Unable to make insert operation";
}
if (!query.exec("SELECT * FROM addressbook;"))
{
qDebug() << "Unable to execute query — exiting";
return 1;
}

// Считываем данные из базы
QSqlRecord rec = query.record();
int nNumber = 0;
QString strName;
QString strPhone;
QString strEmail;
//int kvo = 0;
while (query.next())
{
//kvo++;
nNumber = query.value(rec.indexOf("number")).toInt();
strName = query.value(rec.indexOf("name")).toString();
strPhone = query.value(rec.indexOf("phone")).toString();
strEmail = query.value(rec.indexOf("email")).toString();
qDebug() << nNumber << " " << strName << ";\t"
<< strPhone << ";\t" << strEmail;
}

str = "SELECT COUNT(*) FROM addressbook WHERE name = 'Piggy' ";
if (!query.exec(str))
{
qDebug() << "Unable to make insert operation";
}
query.first();
qDebug() << "kolichestvo strok = "<< query.value(0).toInt();

return 0;
}
