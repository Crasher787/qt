
#include <QCoreApplication>

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <openssl/evp.h>

//#define BUFSIZE 1024

/* General encryption and decryption function example using FILE I/O and AES128 with a 128-bit key. */

int do_crypt(FILE *in, FILE *out, int do_encrypt){
    /* Allow enough space in output buffer for additional block */
    unsigned char inbuf[1024], outbuf[1024 + EVP_MAX_BLOCK_LENGTH];
    int inlen, outlen;

    /* Bogus key and IV: we'd normally set these from another source. */
    unsigned char key[] = "0123456789abcdeF";
    unsigned char iv[] = "1234567887654321";

    EVP_CIPHER_CTX *ctx;
    ctx = EVP_CIPHER_CTX_new();

    /* Don't set key or IV right away; we want to check lengths */
    EVP_CipherInit_ex(ctx, EVP_aes_128_cbc(), NULL, NULL, NULL, do_encrypt);
    OPENSSL_assert(EVP_CIPHER_CTX_key_length(ctx) == 16);
    OPENSSL_assert(EVP_CIPHER_CTX_iv_length(ctx) == 16);

    /* Now we can set key and IV */
    EVP_CipherInit_ex(ctx, NULL, NULL, key, iv, do_encrypt);

    for(;;){
        inlen = fread(inbuf, 1, 1024, in);
        if (inlen <= 0) break;
        if(!EVP_CipherUpdate(ctx, outbuf, &outlen, inbuf, inlen)){
            /* Error */
            EVP_CIPHER_CTX_free(ctx);
            return 0;
        }
        fwrite(outbuf, 1, outlen, out);
    }

    if(!EVP_CipherFinal_ex(ctx, outbuf, &outlen)){
        /* Error */
        EVP_CIPHER_CTX_free(ctx);
        return 0;
    }
    fwrite(outbuf, 1, outlen, out);

    EVP_CIPHER_CTX_free(ctx);
    return 1;
}

int main(int argc, char *argv[]){
    QCoreApplication a(argc, argv);

    //############

    //setlocale(LC_ALL, "Russian");

    //FILE *encode_file = fopen("C://Users/victo/Documents/qweweqw/eqwe.txt", "r");
   // FILE *decode_file = fopen("C://Users/victo/Documents/qweweqw/out.txt", "w+");

    //do_crypt(encode_file, decode_file, 1);
    // 0 - decrypt, 1 - encrypt

    FILE *encode_file = fopen("C:\\Users\victo\Documents\qweweqw\out.txt", "r");
    FILE *decode_file = fopen("C:\\Users\victo\Documents\qweweqw\out1.txt", "w+");

    do_crypt(encode_file, decode_file, 0); // 0 - decrypt, 1 - encrypt */

    //############

    return a.exec();
}
